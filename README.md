Project shows different approaches for creating authorization server with standard protocol Oauth2.0

this project contains subjects:

- [password grant type](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/passwordGrantType/)
- [authorization code grant type](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/authorizationCodeGrantType/)
- [client credentials grant type](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/clientCredentilasGrantType/)
- [tokenstore approach](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/tokenStore/)
- [JWT symmetric key](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/jwtSymmetricKey/)
- [JWT asymmetric key](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/jwtAsymmetricKey/)
- [JWT asymmetric key with keys only on authorization server side](https://bitbucket.org/folusz-sebastian/authorizationserveroauth2/src/jwtAsymmetricKeyWithKeysOnlyOnAuthSide/)

Project was created based on "Spring Security in Action" book written by Laurențiu Spilcă.
